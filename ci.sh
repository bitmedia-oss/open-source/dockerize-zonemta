#!/bin/sh
## This script will not work in bash, only in zsh (alpine image default).

set -o pipefail
set -o errexit

if [ -n "$CI_REGISTRY_USER" ]; then
    docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
fi

if [ -z "$CI_REGISTRY_IMAGE" ]; then
    CI_REGISTRY_IMAGE='registry.gitlab.com/bitmedia-oss/open-source/dockerize-zonemta'
fi

export TAG=dev
if [ -n "$CI_COMMIT_REF_SLUG" ]; then
    TAG=wip-$CI_COMMIT_REF_SLUG
fi
if [ -n "$CI_COMMIT_TAG" ]; then
    TAG=$CI_COMMIT_TAG
fi
API_IMG_TAG=$CI_REGISTRY_IMAGE/zonemta-api:$TAG
WEBADMIN_IMG_TAG=$CI_REGISTRY_IMAGE/zonemta-webadmin:$TAG
echo "Building $IMG_TAG"


if [ -n "$CI_REGISTRY_USER" ]; then
    LoadOrPush='--push'
    BuildXDriver='docker-container'
    Platform='--platform linux/arm64,linux/amd64'
else
    Platform=''
    LoadOrPush='--load'
    BuildXDriver='docker'
fi

docker buildx create --name grow --bootstrap --use --driver=$BuildXDriver || echo "already there"


docker buildx build $Platform --build-arg CI_COMMIT_TAG=$CI_COMMIT_TAG \
    --build-arg CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME \
    --build-arg CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG \
    --build-arg CI_COMMIT_SHA=$CI_COMMIT_SHA \
	-f ./api/Dockerfile \
    -t $API_IMG_TAG \
    $LoadOrPush \
    api

docker buildx build $Platform --build-arg CI_COMMIT_TAG=$CI_COMMIT_TAG \
    --build-arg CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME \
    --build-arg CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG \
    --build-arg CI_COMMIT_SHA=$CI_COMMIT_SHA \
	-f ./webadmin/Dockerfile \
    -t $WEBADMIN_IMG_TAG \
    $LoadOrPush \
    webadmin

