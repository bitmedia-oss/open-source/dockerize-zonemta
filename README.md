# ZoneMTA

This repository builds the docker images for ZoneMTA and ZoneMTA web admin, with a specific, developer friendly, configuration:

* No Security / Authentication
* No Storage / No Volumes
* No actual delivery
* Redis configured to hostname `zonemta-redis`
* Mongo configured to hostname `zonemta-mongo`
* Api configured to hostname `zonemta-api`
* Mailhog configure to hostname `mailhog`
* WebAmin configured to hostname `zonemta-webadmin`, with user `admin` and `secretpass`


Images built:

* registry.gitlab.com/bitmedia-oss/open-source/dockerize-zonemta/zonemta-api
* registry.gitlab.com/bitmedia-oss/open-source/dockerize-zonemta/zonemta-webadmin

Sample docker-compose.yml, which exposes port 12080 (API) and 8082 (WebAdmin), included in this repo:

```yml
version: '3.8'
services:
  mailhog:
    image: mailhog/mailhog:v1.0.1
    expose:
      - 1025
      - 8025
    ports:
      - "1025:1025"
      - "8025:8025"
  zonemta-api:
    image: ${ZONEMTA_API_IMG:-registry.gitlab.com/bitmedia-oss/open-source/dockerize-zonemta/zonemta-api}:${ZONEMTA_API_VERSION:-wip-main}
    expose:
      - 12080
      - 2525
    ports:
      - "12080:12080"
      - "2525:2525"
  zonemta-webadmin:
    image: ${ZONEMTA_WEBADMIN_IMG:-registry.gitlab.com/bitmedia-oss/open-source/dockerize-zonemta/zonemta-webadmin}:${ZONEMTA_WEBADMIN_VERSION:-wip-main}
    expose:
      - 8082
      - 31239
    ports:
      - "8082:8082"
  zonemta-redis:
    image: redis:6
    expose:
      - 6379
  zonemta-mongo:
    image: mongo
    expose:
      - 27017 
```


Run it:

```
docker-compose up -d 
```

Test it:

```
./sample-email.sh
```
(Output should be something like)

```
{"id":"17a9b094a2d00051f7","from":"sender@example.com","to":["recipient1@example.com","recipient2@example.com"],"response":"Message queued as 17a9b094a2d00051f7"}
```

* Open http://localhost:8025 (mailhog) - the message should be there
* Open http://localhost:8082 (zonemta webadmin) - and enter the id `17a9b094a2d00051f7` - it should say `QUEUED` and `ACCEPTED`

